from django.urls import re_path

from . import views

urlpatterns = [

    re_path(r'^exchange_rates/$', views.exchange_rate),
    re_path('', views.index_load, name='index'),


]