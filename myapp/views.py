from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import (api_view, permission_classes)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from pandas import read_csv
from matplotlib import pyplot
from statsmodels.tsa.arima_model import ARIMA
from datetime import datetime,timedelta
import requests
import numpy as np
import pprint
# from sklearn.metrics import MeanSquaredError
# GET https://api.exchangeratesapi.io/history?start_at=2018-01-01&end_at=2018-09-01&base=USD HTTP/1.1



def index_load(request):
    # response = requests.get("https://api.exchangeratesapi.io/history?start_at=2019-07-26&end_at=2019-07-27&base=USD&symbols=INR")
    # data= response.json()
    # pp = pprint.PrettyPrinter(indent=4)
    #
    # pp.pprint(data)
    return render(request, 'index.html', {})



@api_view(['POST'])
def exchange_rate(request):
    def StartARIMAForecasting(Actual, P, D, Q):
        model = ARIMA(Actual, order=(P, D, Q))
        model_fit = model.fit(disp=0)
        prediction = model_fit.forecast()[0]

        return prediction

    data=request.data
    base=data['base']
    target=data['target']
    amount=data['amount']
    waiting=data['waiting']
    end_date_text=data['start_date']

    end_date=datetime.strptime(end_date_text,"%Y-%m-%d")

    startdate = (end_date + timedelta(days=-60))
    start_date=startdate.date()

    url="https://api.exchangeratesapi.io/history?start_at="+str(start_date)+"&end_at="+str(end_date_text)+"&base="+base+"&symbols="+target
    print(url)
    response = requests.get(url)
    response_json=response.json()

    training_data=[]

    for key, value in response_json['rates'].items():
        training_data.append(np.array([value[target]]))

    # print("The full array {}".format(training_data))
    # test_train=training_data[0:40]
    # print("test train array {}".format(test_train))
    # test_data=training_data[40:len(training_data)]
    # print("Tes data arreay {}".format(test_data))
    # predictions=[]
    # for number in range(len(test_data)):
    #     actual_data=test_data[number]
    #     print("Actual data in test {}".format(actual_data))
    #     predict = StartARIMAForecasting(test_train, 3, 1, 0)
    #     print("Actual data in test {} predict {}".format(actual_data,predict))
    #     predictions.append(predict)
    #     test_train.append(actual_data)
    predictions = {}
    tabel=[]

    for number in range(int(waiting)):

        predict_date=(end_date + timedelta(days=number))

        val=predict_date.weekday()
        print("Val {}".format(val))
        if val<5:

            print("True it is week day {}".format(predict_date.date()))

            predict = StartARIMAForecasting(training_data, 3, 1, 0)
            predictions[str(predict_date.date())]=predict[0]

            estimated_return=float(predict[0]) * float(amount)
            table_data = {}


            table_data['date']=str(predict_date.date())
            table_data['rate'] = predict[0]
            table_data['estimated_return'] = estimated_return
            tabel.append(table_data)

            training_data.append(predict)
        else:
            print("not a week day")
            continue

    print("The prediction are {}".format(predictions))
    print("Table data {}".format(tabel))
    response={}
    response['predictions']=predictions
    response['table_date']=tabel
    return Response(response)




# def index(request):
#     def StartARIMAForecasting(Actual, P, D, Q):
#         model = ARIMA(Actual, order=(P, D, Q))
#         model_fit = model.fit(disp=0)
#         prediction = model_fit.forecast()[0]
#         print("insde prediction {}".format(prediction))
#         return prediction
#
#     def GetData(fileName):
#         return read_csv(fileName, header=0, parse_dates=[0], index_col=0).values
#
#     filename=open("/Users/mohitjain/projects/predict/myapp/exchange.csv")
#     ActualData = GetData(filename)
#     print("CSV DATA {}".format(ActualData))
#     NumberOfElements = len(ActualData)
#
#     TrainingSize = int(NumberOfElements * 0.7)
#
#     TrainingData = ActualData[0:TrainingSize]
#     #    TrainingData=[[12][11][9][15]]
#     print(TrainingData)
#
#     TestData = ActualData[TrainingSize:NumberOfElements]
#
#     Actual = [x for x in TrainingData]
#
#     print("Actual Array {}".format(Actual))
#     Predictions = list()
#     for timepoint in range(len(TestData)):
#
#         # forcast value
#         Prediction = StartARIMAForecasting(Actual, 1, 0,0 )
#         print('Actual=%f, Predicted=%f' % (ActualValue, Prediction))
#         # add it in the list
#         Predictions.append(Prediction)
#         Actual.append(ActualValue)
#
#     print("Predictions {}".format(Predictions) )
#
#
#
#
#     return HttpResponse("Hello, world. You're at the polls index.")

